//
// Created by dawid on 06.07.16.
//

#include "PlayerSprite.h"
#include <iostream>
#include "MainSDL.h"

class MainSDL;

PlayerSprite::PlayerSprite() {
  mVelX = 0;
  mVelY = 0;
}
PlayerSprite::~PlayerSprite() {

}
void PlayerSprite::loadGraphic(std::string& imagePathParam) {
  AnimateGraphicItem::loadGraphic( imagePathParam );
}
void PlayerSprite::render() {
  changeTexture();
  AnimateGraphicItem::render();

  if(MainSDL::S_DEBUG) std::clog << mPosX << " : " << mPosY << std::endl;
}
void PlayerSprite::move(float time) {
  mPosX += mVelX * time;


  if(mPosX < 0) {
    mPosX = 0;
  } else if((mPosX + mCollisionBox.w > MainSDL::WINDOW_WIDTH) || mCollision) {
    mPosX -= mVelX;
    mCollisionBox.x = mPosX;
  }

//  mPosY += mVelY * time;

  if(mPosY < 0) {
    mPosY = 0;
  } else if((mPosY + mCollisionBox.h > MainSDL::WINDOW_HEIGHT) || mCollision) {
    mPosY -= mVelY;
    mCollisionBox.y = mPosY;
  }
  if (!mCollision) {
    mPosY += 1;
  }
}
void PlayerSprite::handleEvent(SDL_Event &eventParam) {
  if (eventParam.type == SDL_KEYUP && eventParam.key.repeat == 0) {
    switch (eventParam.key.keysym.sym) {
      case SDLK_UP:
        mVelY += 0;
        break;
      case SDLK_DOWN:
        mVelY -= 0;
        break;
      case SDLK_LEFT:
        mVelX += PLAYER_VELOCITY;
        mRun = false;
        break;
      case SDLK_RIGHT:
        mVelX -= PLAYER_VELOCITY;
        mRun = false;
        break;
    }
  } else
    if (eventParam.type == SDL_KEYDOWN && eventParam.key.repeat == 0) {
      switch (eventParam.key.keysym.sym) {
        case SDLK_UP:
          mVelY -= 0;
          break;
        case SDLK_DOWN:
          mVelY += 0;
          break;
        case SDLK_LEFT:
          mVelX -= PLAYER_VELOCITY;
          mRun = true;
          break;
        case SDLK_RIGHT:
          mVelX += PLAYER_VELOCITY;
          mRun = true;
          break;
      }
    }
}
void PlayerSprite::changeTexture() {
  if(mRun) {
    mOwnTexture = mTexturesVec[mCounter];

    ++mCounter;

    if(mCounter >= mTexturesVec.size())
      mCounter = 0;
  } else {
    mCounter = 0;
    mOwnTexture = mTexturesVec[mCounter];
  }
}