//
// Created by dawid on 06.07.16.
//

#include "GraphicItem.h"
#include "MainSDL.h"

void GraphicItem::render() {
  mOwnTexture->render(mPosX, mPosY);

  if(MainSDL::S_DEBUG) {
    SDL_Rect controlRect;
    controlRect.h = mOwnTexture->getMHeight();
    controlRect.w = mOwnTexture->getMWidth();
    controlRect.x = (int)mPosX;
    controlRect.y = (int)mPosY;


    SDL_SetRenderDrawColor(MainSDL::getSdlRenderer(), 0x12, 0xFF, 0x22, 0x00);
    SDL_RenderDrawRect(MainSDL::getSdlRenderer(), &controlRect);
  }
}
GraphicItem::GraphicItem() {
  mOwnTexture = new TextureSDL();

  mPosY = 0;
  mPosX = 0;

  mCollisionBox.w = 0;
  mCollisionBox.h = 0;

  mCollision = false;
}
GraphicItem::GraphicItem(std::string &pathParam) {
  mOwnTexture = new TextureSDL();
  mOwnTexture->loadFile(pathParam);
  GraphicItem::mTexturePath = pathParam;

  mPosY = 0;
  mPosX = 0;

  mCollisionBox.w = mOwnTexture->getMWidth();
  mCollisionBox.h = mOwnTexture->getMHeight();
}
GraphicItem::~GraphicItem() {
  if(mOwnTexture != nullptr) delete mOwnTexture;
}
void GraphicItem::loadGraphic(std::string& imagePath) {
  mOwnTexture->loadFile(imagePath);
  GraphicItem::mTexturePath = imagePath;
  mCollisionBox.w = mOwnTexture->getMWidth();
  mCollisionBox.h = mOwnTexture->getMHeight();
}
GraphicItem::GraphicItem(const GraphicItem& copyObj) {
  mTexturePath = copyObj.mTexturePath;

  mOwnTexture = new TextureSDL();
  mOwnTexture->loadFile(mTexturePath);

  mPosX = copyObj.mPosX;
  mPosY = copyObj.mPosY;

  mCollisionBox = copyObj.mCollisionBox;
  mCollision = copyObj.mCollision;
}
void GraphicItem::setMPosX(float mPosX) {
  GraphicItem::mPosX = mPosX;
}
void GraphicItem::setMPosY(float mPosY) {
  GraphicItem::mPosY = mPosY;
}
void GraphicItem::checkCollision(const GraphicItem &collisionObject) {
  int topThis, rightThis, bottomThis, leftThis;
  int topOther, rightOther, bottomOther, leftOther;

  topThis = mPosY;
  rightThis = mPosX + mCollisionBox.w;
  bottomThis = mPosY + mCollisionBox.h;
  leftThis = mPosX;

  topOther = collisionObject.mPosY;
  rightOther = collisionObject.mPosX + collisionObject.mCollisionBox.w;
  bottomOther = collisionObject.mPosY + collisionObject.mCollisionBox.h;
  leftOther = collisionObject.mPosX;

  mCollision = true;

  if (topThis >= bottomOther)
    mCollision = false;
  if (rightThis <= leftOther)
    mCollision = false;
  if (bottomThis <= topOther)
    mCollision = false;
  if (leftThis >= rightOther)
    mCollision = false;
}
void GraphicItem::fixColliderX(int xParam) {
  mCollisionBox.x += xParam;
}
void GraphicItem::fixColliderY(int yParam) {
  mCollisionBox.y += yParam;
}
void GraphicItem::fixColliderW(int wParam) {
  mCollisionBox.w += wParam;
}
void GraphicItem::fixColliderH(int hParam) {
  mCollisionBox.h += hParam;
}
