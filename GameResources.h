//
// Created by dawid on 19.07.16.
//

#pragma once

#include <vector>
#include "GraphicItem.h"
#include "AnimateGraphicItem.h"
#include "PlayerSprite.h"
namespace GameRC{

enum class Type {Player, Enemy, BG_Sprites};
enum class BG_SpriteType {Floor};

using resourceGraphicItem = std::pair<GameRC::Type, GraphicItem>;
using resourceAnimateGraphicItem = std::pair<GameRC::Type, AnimateGraphicItem>;

class GameResources {
   private:
    std::vector<resourceGraphicItem> mStaticsObjects;
    std::vector<resourceAnimateGraphicItem> mAnimObjects;

    PlayerSprite* mPlayerObject;
   public:
    PlayerSprite *getMPlayerObject();
    void setMPlayerObject(PlayerSprite *mPlayerObject);

    virtual ~GameResources();
    void Add(const GraphicItem &graphicParam, GameRC::Type typeParam);
    void Add(const AnimateGraphicItem &aGraphicParam, GameRC::Type typeParam);

    std::vector<resourceGraphicItem>& getResourcesStatic();
    std::vector<resourceAnimateGraphicItem>& getResourcesAnimated();
};
}