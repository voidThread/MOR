//
// Created by dawid on 06.07.16.
//

#pragma once


#include <SDL2/SDL_render.h>
#include <string>
class TextureSDL {
 private:
  friend class MainSDL;

  SDL_Texture* mTexture;

  int mWidth;
  int mHeight;

 public:
  TextureSDL();
  TextureSDL(const TextureSDL&);
  virtual ~TextureSDL();

  bool loadFile(std::string pathParam);
  void setColor(Uint8 r, Uint8 g, Uint8 b);
  void setBlendMode(SDL_BlendMode &blendingParam);
  void setAlpha(Uint8 a);

  void render(int x, int y, SDL_Rect* clipParam = nullptr, double angleParam = 0.0, SDL_Point* centerParam = nullptr,
              SDL_RendererFlip flipParam = SDL_FLIP_NONE);

  int getMWidth() const;
  int getMHeight() const;
};

