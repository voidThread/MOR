cmake_minimum_required(VERSION 3.5)
project(PeanutButter)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp MainSDL.cpp MainSDL.h ItemInterface.h GraphicItem.cpp GraphicItem.h TextureSDL.cpp TextureSDL.h PlayerSprite.cpp PlayerSprite.h AnimateGraphicItem.cpp AnimateGraphicItem.h TimerSDL.cpp TimerSDL.h GameResources.cpp GameResources.h)
add_executable(PeanutButter ${SOURCE_FILES})

INCLUDE(FindPkgConfig)

PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
PKG_SEARCH_MODULE(SDL2IMAGE REQUIRED SDL2_image>=2.0.0)

INCLUDE_DIRECTORIES(${SDL2_INCLUDE_DIRS} ${SDL2IMAGE_INCLUDE_DIRS})
TARGET_LINK_LIBRARIES(${PROJECT_NAME} ${SDL2_LIBRARIES} ${SDL2IMAGE_LIBRARIES})

add_custom_command(TARGET PeanutButter PRE_BUILD COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_SOURCE_DIR}/Resources $<TARGET_FILE_DIR:PeanutButter>/Resources)