//
// Created by dawid on 06.07.16.
//

#include <SDL2/SDL_image.h>
#include <iostream>
#include "TextureSDL.h"
#include "MainSDL.h"

TextureSDL::TextureSDL() {
  mTexture = nullptr;
  mHeight = 0;
  mWidth = 0;
}
TextureSDL::TextureSDL(const TextureSDL& textureParam) {
  mTexture = textureParam.mTexture;
  mHeight = textureParam.mHeight;
  mWidth = textureParam.mWidth;
}
TextureSDL::~TextureSDL() {
  if (mTexture != nullptr) {
    SDL_DestroyTexture(mTexture);
    mTexture = nullptr;
    mWidth = 0;
    mHeight = 0;
  }
}
bool TextureSDL::loadFile(std::string pathParam) {
  using namespace std;

  auto loadFlag = false;
  auto surface = IMG_Load(pathParam.c_str());

  if (surface == nullptr) {
    cerr << "Can't load file " << pathParam << " " << SDL_GetError() << endl;
  } else {
    auto newTexture = SDL_CreateTextureFromSurface(MainSDL::getSdlRenderer() , surface);

    if (newTexture == nullptr) {
      cerr << "Can't create texture!" << pathParam << " " << SDL_GetError() << endl;
    } else {
      mTexture = newTexture;
      mHeight = surface->h;
      mWidth = surface->w;

      loadFlag = true;
    }

    SDL_FreeSurface(surface);
  }

  return loadFlag;
}
int TextureSDL::getMWidth() const {
  return mWidth;
}
int TextureSDL::getMHeight() const {
  return mHeight;
}
void TextureSDL::setColor(Uint8 r, Uint8 g, Uint8 b) {
  SDL_SetTextureColorMod(mTexture, r, g, b);
}
void TextureSDL::setBlendMode(SDL_BlendMode &blendingParam) {
  SDL_SetTextureBlendMode(mTexture, blendingParam);
}
void TextureSDL::setAlpha(Uint8 a) {
  SDL_SetTextureAlphaMod(mTexture, a);
}
void TextureSDL::render(int x,
                        int y,
                        SDL_Rect *clipParam,
                        double angleParam,
                        SDL_Point *centerParam,
                        SDL_RendererFlip flipParam) {

  SDL_Rect renderRect = {x, y, mWidth, mHeight};

  if (clipParam != nullptr) {
    renderRect.w = clipParam->w;
    renderRect.h = clipParam->h;
  }

  SDL_RenderCopyEx(MainSDL::getSdlRenderer(), mTexture, clipParam, &renderRect, angleParam, centerParam, flipParam);
}