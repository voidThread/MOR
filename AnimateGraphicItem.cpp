//
// Created by dawid on 11.07.16.
//

#include "AnimateGraphicItem.h"

AnimateGraphicItem::AnimateGraphicItem() {

}
AnimateGraphicItem::~AnimateGraphicItem() {
  for(auto&& texture : mTexturesVec) {
    delete texture;
    texture = nullptr;
  }

  mOwnTexture = nullptr;
}
void AnimateGraphicItem::loadGraphic(std::string &pathParam) {
  TextureSDL* newTexture = new TextureSDL;
  newTexture->loadFile(pathParam);

  //!< take max size of all textures
  if (mCollisionBox.h < newTexture->getMHeight()) mCollisionBox.h = newTexture->getMHeight();
  if (mCollisionBox.w < newTexture->getMWidth()) mCollisionBox.w = newTexture->getMWidth();

  mTexturesVec.push_back(newTexture);
}
void AnimateGraphicItem::render() {
  GraphicItem::render();
}
void AnimateGraphicItem::checkCollision(const GraphicItem &itemParam) {
  return GraphicItem::checkCollision(itemParam);
}
