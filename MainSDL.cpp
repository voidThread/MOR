//
// Created by dawid on 05.07.16.
//

#include "MainSDL.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include "GameResources.h"

SDL_Window* MainSDL::sSdlWindow;
SDL_Renderer* MainSDL::sSdlRenderer;

MainSDL::~MainSDL() {
  SDL_DestroyWindow(sSdlWindow);
  SDL_DestroyRenderer(sSdlRenderer);

  sSdlWindow = nullptr;
  sSdlRenderer = nullptr;

  IMG_Quit();
  SDL_Quit();
}
bool MainSDL::Init() {
  auto returnFlag = false;

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cerr << "SDL init error!" << SDL_GetError() << std::endl;
  } else {
    sSdlWindow = SDL_CreateWindow(mMainTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
//    sSdlWindow = SDL_CreateWindow(mMainTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

    if (sSdlWindow == nullptr) {
      std::cerr << "Window create error!" << SDL_GetError() << std::endl;
    } else {
      sSdlRenderer = SDL_CreateRenderer(sSdlWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
//      sSdlRenderer = SDL_CreateRenderer(sSdlWindow, -1, SDL_RENDERER_PRESENTVSYNC);

      if (sSdlRenderer == nullptr) {
        std::cerr << "Renderer create error!" << SDL_GetError() << std::endl;
      } else {
        SDL_SetRenderDrawColor(sSdlRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

        auto imgFlags = IMG_INIT_PNG;

        if (!( IMG_Init(imgFlags) & imgFlags )) {
          std::cerr << "SDL_image init error!" << SDL_GetError() << std::endl;
        }

        returnFlag = true;
        mInterpolation = 0;
        mNextGameUpdate = 0;
        mUpdateGameLoopNumber = 0;
      }
    }
  }

  return returnFlag;
}
void MainSDL::LoadResources() {
  std::string path = "Resources/peanut_defX3.png";

  PlayerSprite* player = new PlayerSprite;
  player->loadGraphic(path);
  path = "Resources/peanut1.png";
  player->loadGraphic(path);
  path = "Resources/peanut2.png";
  player->loadGraphic(path);
  path = "Resources/peanut3.png";
  player->loadGraphic(path);

  mainRC.setMPlayerObject(player);

  path = "Resources/ground.png";

  GraphicItem groundGrap;
  groundGrap.loadGraphic(path);
//  TODO move in place where setup is scene
  groundGrap.setMPosX(0);
  groundGrap.setMPosY(WINDOW_HEIGHT-72);
  groundGrap.fixColliderY(+2);
  groundGrap.fixColliderH(-2);

  mainRC.Add(groundGrap, GameRC::Type::BG_Sprites);
}
bool MainSDL::EventsHandle() {
  auto quitFlag = false;
  while (SDL_PollEvent(&sdlEvent) != 0) {
    auto escapeKeyPress = (sdlEvent.type == SDL_KEYUP && (sdlEvent.key.keysym.sym == SDLK_ESCAPE));
    if (sdlEvent.type == SDL_QUIT || escapeKeyPress) {
      quitFlag = true;
    }
    //!<handle input here
    mainRC.getMPlayerObject()->handleEvent(sdlEvent);

  }

  return quitFlag;
}
void MainSDL::UpdateGame() {
  mUpdateGameLoopNumber = 0;
  mNextGameUpdate = 0;
  std::clog << "Ticks: " << stepTime.getTicks()
      << "\tNextGameUpdate: " << mNextGameUpdate
      << "\tLoopNumber: " << mUpdateGameLoopNumber
            << "\tFrameSkip: " << sMaxFrameSkip << std::endl;
  while (stepTime.getTicks() > mNextGameUpdate && mUpdateGameLoopNumber < sMaxFrameSkip) {
    calcCollisions();

    mUpdateGameLoopNumber++;
    mNextGameUpdate += sUpdateSkip;
  }

  mInterpolation = float(stepTime.getTicks() + sUpdateSkip - mNextGameUpdate) / float(sUpdateSkip);
  mainRC.getMPlayerObject()->move(mInterpolation);
//  TODO: add break game when player dropdown
  stepTime.start();
}
void MainSDL::Render() {
  //!< clear screen
  SDL_SetRenderDrawColor(sSdlRenderer, 0xFF, 0xFF, 0xFF, 0xFF);
  SDL_RenderClear(sSdlRenderer);

  //!< here render

  //!< draw bottom line
  SDL_SetRenderDrawColor(sSdlRenderer, 0xFF, 0x10, 0xFF, 0xFF);
  SDL_RenderDrawLine(sSdlRenderer, 0, MainSDL::WINDOW_HEIGHT-24, MainSDL::WINDOW_WIDTH, MainSDL::WINDOW_HEIGHT-24);

  //!< draw player
  renderResources();

  //!< update renderer
  SDL_RenderPresent(sSdlRenderer);
}
void MainSDL::calcCollisions() {
  for (GameRC::resourceGraphicItem& resource : mainRC.getResourcesStatic()) {
    mainRC.getMPlayerObject()->checkCollision(resource.second);
  }
}
void MainSDL::renderResources() {
  for (GameRC::resourceGraphicItem& resource : mainRC.getResourcesStatic()) {
    resource.second.render();
  }

  for (GameRC::resourceAnimateGraphicItem& resource : mainRC.getResourcesAnimated()) {
    resource.second.render();
  }

  mainRC.getMPlayerObject()->render();
}
