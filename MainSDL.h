//
// Created by dawid on 05.07.16.
//
#pragma once

#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_events.h>
#include "GameResources.h"
#include "TimerSDL.h"

class MainSDL {
 private:
  SDL_Event sdlEvent;
  GameRC::GameResources mainRC;
  TimerSDL stepTime;

 protected:
  static SDL_Window* sSdlWindow;
  static SDL_Renderer* sSdlRenderer;

  static const short sUpdatesPerSecond = 15;
  static const short sUpdateSkip = 1000 / sUpdatesPerSecond;
  static const short sMaxFrameSkip = 5;

  int mUpdateGameLoopNumber;
  float mInterpolation, mNextGameUpdate;

  std::string mMainTitle = "PeanutButter";

  void calcCollisions();
  void renderResources();

 public:
  static const unsigned int WINDOW_HEIGHT = 320;
  static const unsigned int WINDOW_WIDTH = 640;

  static const bool S_DEBUG = true;

  virtual ~MainSDL();
  bool Init();

  void LoadResources();
  bool EventsHandle();
  void UpdateGame();
  void Render();

  static SDL_Renderer *getSdlRenderer() {
    return sSdlRenderer;
  }
};

