//
// Created by dawid on 06.07.16.
//

#include "ItemInterface.h"
#include "TextureSDL.h"

#pragma once

class GraphicItem : public Item {
 protected:
  float mPosX, mPosY;
  TextureSDL* mOwnTexture;

  std::string mTexturePath;

  SDL_Rect mCollisionBox;
  bool mCollision;

 public:
  GraphicItem();
  GraphicItem(std::string& pathParam);
  GraphicItem(const GraphicItem&);
  virtual ~GraphicItem();

  virtual void loadGraphic(std::string& imagePath);
  virtual void render();
  void setMPosX(float mPosX);
  void setMPosY(float mPosY);
  void fixColliderX(int);
  void fixColliderY(int);
  void fixColliderW(int);
  void fixColliderH(int);

  virtual void checkCollision(const GraphicItem &);
};