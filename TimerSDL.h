//
// Created by dawid on 11.07.16.
//

#pragma once


#include <SDL2/SDL_quit.h>

class TimerSDL {
 protected:
  bool mPaused;
  bool mStarted;

  Uint32 mStartTicks;
  Uint32 mPausedTicks;

 public:
  TimerSDL();
  TimerSDL(const TimerSDL& timerParam) = delete;
  virtual ~TimerSDL();

  void start();
  void stop();
  void pause();
  void unPause();

  bool isStarted();
  bool isPaused();

  Uint32 getTicks();
};