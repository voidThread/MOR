//
// Created by dawid on 06.07.16.
//

#pragma once

#include <SDL2/SDL_events.h>
#include <vector>
#include "AnimateGraphicItem.h"

class PlayerSprite : public AnimateGraphicItem{
 protected:
  float mVelX, mVelY;
  static const int PLAYER_VELOCITY = 30;

  bool mRun = false;
  unsigned int mCounter = 0;

  void changeTexture();

 public:
  PlayerSprite();
  PlayerSprite(const PlayerSprite&) = default;
  virtual ~PlayerSprite();

  virtual void loadGraphic(std::string& imgPath);
  virtual void render();
  virtual void move(float time);

  void handleEvent(SDL_Event& eventParam);
};