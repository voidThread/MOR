//
// Created by dawid on 19.07.16.
//

#include "GameResources.h"
void GameRC::GameResources::Add(const GraphicItem &graphicParam, GameRC::Type typeParam) {
  GameResources::mStaticsObjects.push_back(std::make_pair(typeParam, graphicParam));
}
void GameRC::GameResources::Add(const AnimateGraphicItem &animGraphParam, GameRC::Type typeParam) {
  GameResources::mAnimObjects.emplace_back(std::make_pair(typeParam, animGraphParam));
}
GameRC::GameResources::~GameResources() {
  if(GameResources::mPlayerObject != nullptr)
    delete GameResources::mPlayerObject;
}
PlayerSprite *GameRC::GameResources::getMPlayerObject() {
  return mPlayerObject;
}
void GameRC::GameResources::setMPlayerObject(PlayerSprite *mPlayerObject) {
  GameResources::mPlayerObject = mPlayerObject;
}
std::vector<GameRC::resourceGraphicItem> &GameRC::GameResources::getResourcesStatic() {
  return mStaticsObjects;
}
std::vector<GameRC::resourceAnimateGraphicItem> &GameRC::GameResources::getResourcesAnimated() {
  return mAnimObjects;
}
