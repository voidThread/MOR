//
// Created by dawid on 11.07.16.
//

#pragma once

#include <vector>
#include "GraphicItem.h"

class AnimateGraphicItem : protected GraphicItem {
 protected:
  std::vector<TextureSDL*> mTexturesVec;

 public:
  AnimateGraphicItem();
  AnimateGraphicItem(const AnimateGraphicItem& copyParam) = default;
  virtual ~AnimateGraphicItem();

  virtual void loadGraphic(std::string& pathParam);
  virtual void render();
  virtual void checkCollision(const GraphicItem &);
};