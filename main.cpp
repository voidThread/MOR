#include "MainSDL.h"
#include <iostream>

using namespace std;
int main() {

  MainSDL rootEngine;

  //!< initialization
  if (rootEngine.Init()) {

    //!< here load all stuff
    rootEngine.LoadResources();

    //!< main loop
    while (!rootEngine.EventsHandle()) {
      //!< event main loop

      rootEngine.UpdateGame();

      rootEngine.Render();
    }
  }

  return 0;
}