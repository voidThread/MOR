//
// Created by dawid on 06.07.16.
//

#pragma once

class Item {
 public:
  virtual ~Item() = default;
  unsigned int mID;
};