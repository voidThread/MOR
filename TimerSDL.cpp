//
// Created by dawid on 11.07.16.
//

#include <SDL2/SDL_timer.h>
#include "TimerSDL.h"

TimerSDL::TimerSDL() {
  mPaused = false;
  mStarted = false;

  mPausedTicks = 0;
  mStartTicks = 0;
}
TimerSDL::~TimerSDL() {

}
void TimerSDL::start() {
  mStarted = true;
  mPaused = false;

  mStartTicks = SDL_GetTicks();
  mPausedTicks = 0;
}
void TimerSDL::stop() {
  mStarted = false;
  mPaused = true;

  mStartTicks = 0;
  mPausedTicks = 0;
}
void TimerSDL::pause() {
  if (mStarted && !mPaused) {
    mPaused = true;
    mPausedTicks = SDL_GetTicks() - mStartTicks;

    mStartTicks = 0;
  }
}
void TimerSDL::unPause() {
  if(mStarted && mPaused) {
    mPaused = false;
    mStartTicks = SDL_GetTicks() - mPausedTicks;

    mPausedTicks = 0;
  }
}
bool TimerSDL::isStarted() {
  return mStarted;
}
bool TimerSDL::isPaused() {
  return mPaused;
}
Uint32 TimerSDL::getTicks() {
  Uint32 time = 0;

  if(mStarted) { //!< timer running
    if(mPaused) { //!< timer paused
      time = mPausedTicks;
    } else {
      time = SDL_GetTicks() - mStartTicks;
    }
  }
  return time;
}